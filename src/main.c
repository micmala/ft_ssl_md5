/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:45:58 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 19:45:59 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <ft_ssl_md5.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <get_next_line.h>
#include <ft_ssl_tester.h>

t_dispatcher	g_md_cmds[] = {
	{"md5", &manage_md5},
	{"sha256", &manage_sha256},
	{"sha224", &manage_sha224},
	{"sha512", &manage_sha512},
	{"sha384", &manage_sha384},
	{NULL, NULL}
};
t_dispatcher	g_cipher_cmds[] = {
	{NULL, NULL}
};

t_dispatcher	g_stndrt_cmds[] = {
	{NULL, NULL}
};

void			stdin_reading(void)
{
	char *line;
	char **argv;
	char *to_free;

	ft_printf("ft_ssl> ");
	while (get_next_line(STDIN_FILENO, &line))
	{
		to_free = line;
		line = ft_strtrim(line);
		ft_strdel(&to_free);
		if (ft_strnequ(line, "exit", 4))
			exit(EXIT_SUCCESS);
		argv = ft_strsplit(line, ' ');
		if (*line)
			ssl_dispatcher(argv);
		free(argv);
		ft_printf("ft_ssl> ");
	}
}

int				main(int argc, char **argv)
{
	if (argc == 1)
		stdin_reading();
	else
		ssl_dispatcher(++argv);
	return (0);
}
