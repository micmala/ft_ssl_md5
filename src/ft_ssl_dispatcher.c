/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_get_cmd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 17:20:21 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 17:20:22 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_ssl_md5.h>

static t_dispatcher	get_cmd(char *argv)
{
	int				i;
	t_dispatcher	empty;

	i = 0;
	empty.name = NULL;
	empty.f_ptr = NULL;
	while (g_md_cmds[i].name
		|| g_cipher_cmds[i].name
		|| g_stndrt_cmds[i].name)
	{
		if (ft_strequ(argv, g_md_cmds[i].name))
			return (g_md_cmds[i]);
		else if (ft_strequ(argv, g_cipher_cmds[i].name))
			return (g_cipher_cmds[i]);
		else if (ft_strequ(argv, g_stndrt_cmds[i].name))
			return (g_stndrt_cmds[i]);
		i++;
	}
	ft_ssl_usage(argv);
	return (empty);
}

void				ssl_dispatcher(char **argv)
{
	t_dispatcher cmd;

	cmd = get_cmd(*argv);
	cmd.f_ptr(++argv);
}
