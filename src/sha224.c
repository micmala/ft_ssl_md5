/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha224.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 16:58:05 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 16:58:06 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

static void	sha224_init(unsigned long *state)
{
	state[0] = 0xc1059ed8;
	state[1] = 0x367cd507;
	state[2] = 0x3070dd17;
	state[3] = 0xf70e5939;
	state[4] = 0xffc00b31;
	state[5] = 0x68581511;
	state[6] = 0x64f98fa7;
	state[7] = 0xbefa4fa4;
}

void		sha224(unsigned char *msg, size_t msg_len)
{
	char			*digest;
	unsigned long	state[8];
	unsigned int	i;

	sha224_init(state);
	msg = msg_padding(msg, &msg_len, SHA256_CHUNK_SIZE, BIG);
	i = 0;
	while (i < msg_len)
	{
		sha256_transform(state, msg + i);
		i += SHA256_CHUNK_SIZE / 8;
	}
	ft_memdel((void **)&msg);
	digest = strcat_digest(state, 8, SHA224_STATE_TO_PRINT, BIG);
	ft_printf("%s", digest);
	ft_strdel(&digest);
}
