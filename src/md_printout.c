/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md_printout.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:42:51 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 19:42:52 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_ssl_md5.h>

void	print_flag_s(t_md *handler)
{
	if (handler->md_flags.q)
		handler->f_ptr(handler->msg, handler->msg_len);
	else
	{
		if (!handler->md_flags.r)
		{
			ft_printf("%s (", handler->funct_name);
			ft_printf("\"%s\") = ", handler->msg);
		}
		handler->f_ptr(handler->msg, handler->msg_len);
		if (handler->md_flags.r)
			ft_printf(" \"%s\"", handler->msg);
	}
	handler->md_flags.s = false;
	ft_printf("\n");
	ft_memdel((void **)&handler->msg);
}

void	print_file(t_md *handler)
{
	if (handler->md_flags.q)
		handler->f_ptr(handler->msg, handler->msg_len);
	else
	{
		if (!handler->md_flags.r)
		{
			ft_printf("%s (", handler->funct_name);
			ft_printf("%s) = ", handler->filename);
		}
		handler->f_ptr(handler->msg, handler->msg_len);
		if (handler->md_flags.r)
			ft_printf(" %s", handler->filename);
	}
	ft_printf("\n");
	ft_memdel((void **)&handler->msg);
}

void	print_flag_p(t_md *handler)
{
	ft_printf("%s", handler->msg);
	handler->f_ptr(handler->msg, handler->msg_len);
	ft_printf("\n");
	ft_memdel((void **)&handler->msg);
}
