/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_md5_sha.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 17:07:05 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 17:07:06 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

void	manage_md5(char **argv)
{
	t_md md;

	md = bzero_handler();
	md.funct_name = "MD5";
	md.f_ptr = &md5;
	md_start(&md, argv);
}

void	manage_sha256(char **argv)
{
	t_md md;

	md = bzero_handler();
	md.funct_name = "SHA256";
	md.f_ptr = &sha256;
	md_start(&md, argv);
}

void	manage_sha224(char **argv)
{
	t_md md;

	md = bzero_handler();
	md.funct_name = "SHA224";
	md.f_ptr = &sha224;
	md_start(&md, argv);
}

void	manage_sha512(char **argv)
{
	t_md md;

	md = bzero_handler();
	md.funct_name = "SHA512";
	md.f_ptr = &sha512;
	md_start(&md, argv);
}

void	manage_sha384(char **argv)
{
	t_md md;

	md = bzero_handler();
	md.funct_name = "SHA384";
	md.f_ptr = &sha384;
	md_start(&md, argv);
}
