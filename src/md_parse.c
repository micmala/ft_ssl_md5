/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:43:28 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 19:43:29 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_ssl_md5.h>
#include <fcntl.h>
#include <sys/stat.h>

void		md_parse_msg(t_md *handler, char *arg)
{
	struct stat	st;
	int			fd;

	handler->md_flags.has_arg = true;
	if (handler->md_flags.s)
	{
		handler->msg = (unsigned char *)ft_strdup(arg);
		handler->msg_len = ft_strlen(arg);
		print_flag_s(handler);
	}
	else
	{
		if (((fd = open(arg, O_RDONLY)) < 0 || read(fd, 0, 0) < 0))
			return (ft_ssl_file_err(handler->funct_name, arg, strerror(errno)));
		handler->filename = arg;
		stat(arg, &st);
		md_read_from_fd(fd, handler, (size_t)st.st_size);
		print_file(handler);
		close(fd);
	}
}

static void	md_parse_flag(t_md *handler, char ***argv)
{
	char *curr;

	curr = ++**argv;
	while (*curr)
	{
		if (!(ft_strchr("pqrs", *curr) && *curr))
			ft_ssl_flag_err(handler->funct_name, *curr, ERR_OPT);
		handler->md_flags.q += *curr == 'q';
		handler->md_flags.r += *curr == 'r';
		if ((handler->md_flags.s = *curr == 's'))
		{
			if (*(curr + 1))
				return (md_parse_msg(handler, curr + 1));
			ssl_string_option(handler, argv);
		}
		else if (*curr == 'p')
		{
			handler->md_flags.p++;
			ssl_p_option(handler);
		}
		curr++;
	}
}

void		md_start(t_md *handler, char **argv)
{
	while (*argv && (**argv == '-' && *(*argv + 1)))
	{
		md_parse_flag(handler, &argv);
		argv++;
	}
	if (!handler->md_flags.has_stdin &&
	!handler->md_flags.has_arg && *argv == NULL)
	{
		handler->md_flags.has_arg = true;
		handler->md_flags.q = true;
		md_read_from_fd(STDIN_FILENO, handler, READ_BUF);
		print_flag_s(handler);
	}
	while (*argv)
	{
		md_parse_msg(handler, *argv);
		argv++;
	}
	if (!handler->md_flags.has_arg)
		if (handler->md_flags.q || handler->md_flags.r)
			handle_redundant_flag(handler);
}
