/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   msg_padding.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 17:09:29 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 17:09:30 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

#define GET_PAD_LEN(bit, chunk) ((((bit + chunk/8)/chunk)*chunk + chunk)/8)

void			append_len_sha(
		unsigned char *msg, size_t bitlen, size_t pad_len)
{
	int i;

	i = -1;
	while (++i < 8)
		msg[pad_len - (8 - i)] = (unsigned char)(bitlen >> (8 * (7 - i)));
}

void			append_len_md5(
		unsigned char *msg, size_t bitlen, size_t pad_len)
{
	int i;

	i = -1;
	while (++i < 8)
		msg[pad_len - (i + 1)] = (unsigned char)(bitlen >> (8 * (7 - i)));
}

unsigned char	*msg_padding(
		unsigned char *msg, size_t *size, size_t chunk_size, int endian)
{
	size_t			bitlen;
	size_t			pad_len;
	unsigned char	*pad_msg;

	bitlen = (*size) * 8;
	pad_len = GET_PAD_LEN(bitlen, chunk_size);
	pad_msg = (unsigned char *)ft_memalloc(pad_len);
	ft_memcpy(pad_msg, msg, *size);
	pad_msg[*size] = 128;
	(endian == BIG ? append_len_sha : append_len_md5)(pad_msg, bitlen, pad_len);
	*size = pad_len;
	return (pad_msg);
}
