#include "ft_ssl_tester.h"
#include "ft_printf/colors.h"

void run_test_case(const char *format)
{
	char **argv;

	ft_printf("Test input: "BMAG"%s\n"RESET, format);
	argv = ft_strsplit(format, ' ');

	ssl_dispatcher(++argv);
	system(format + 7);

	ft_printf("-------------------------------------------------------------------------------------\n");
}

void openssl_err(const char *format)
{
	char *for_syst;

	for_syst = ft_strdup("openssl ");
	for_syst = ft_strcat(for_syst, format + 7);

	ft_printf("Test input: "BMAG"%s\n"RESET, for_syst);

	system(for_syst);
	run_test_case(format);

}
void test_md5(int ACTION)
{

	if (ACTION == TEST_CASES)
	{
		run_test_case("ft_ssl md5 author");
		run_test_case("ft_ssl md5 -s test");
		run_test_case("ft_ssl md5 -stest");
		run_test_case("ft_ssl md5 -q author");
		run_test_case("ft_ssl md5 -qs author");
		run_test_case("ft_ssl md5 -qsauthor");
		run_test_case("ft_ssl md5 -rsauthor");
		run_test_case("ft_ssl md5 -qrsauthor");
		run_test_case("ft_ssl md5 -qr author");
		run_test_case("ft_ssl md5 -r author");
		run_test_case("ft_ssl md5 -s 56_len_string!..........................................");
	}
	else if (ACTION == OPENSSL_ERR)
	{
		openssl_err("ft_ssl wrong_fucnt -s test");
	}

}