/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 16:41:54 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 16:41:55 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"
#define F(x, y, z) (((x) & (y)) | ((~x) & (z)))
#define G(x, y, z) (((x) & (z)) | ((y) & (~z)))
#define H(x, y, z) ((x) ^ (y) ^ (z))
#define I(x, y, z) ((y) ^ ((x) | (~z)))
#define ROTATE_LEFT(x, n) (((x) << (n)) | ((x) >> (32-(n))))

static const unsigned	g_s[] =
{
	7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
	5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20,
	4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
	6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21
};

static const unsigned	g_k[] =
{
	0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
	0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
	0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
	0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
	0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
	0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
	0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
	0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
	0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
	0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
	0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
	0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
	0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
	0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
	0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
	0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
};

void			md5_init(unsigned long *state)
{
	state[0] = 0x67452301;
	state[1] = 0xefcdab89;
	state[2] = 0x98badcfe;
	state[3] = 0x10325476;
}

static void		md5_transform(unsigned long *state, const unsigned int *data)
{
	unsigned int	vars[4];
	unsigned int	f;
	int				i;
	int				g;

	i = -1;
	while (++i < 4)
		vars[i] = (unsigned int)state[i];
	i = -1;
	while (++i < 64)
	{
		if (i < 16 && (g = i) != -1)
			f = F(vars[1], vars[2], vars[3]);
		else if (i < 32 && (g = (5 * i + 1) % 16) != -1)
			f = G(vars[1], vars[2], vars[3]);
		else if (i < 48 && (g = (3 * i + 5) % 16) != -1)
			f = H(vars[1], vars[2], vars[3]);
		else if ((g = (7 * i) % 16) != -1)
			f = I(vars[1], vars[2], vars[3]);
		md5_swap_words(vars,
				ROTATE_LEFT((vars[0] + f + g_k[i] + data[g]), g_s[i]));
	}
	i = -1;
	while (++i < 4)
		state[i] += vars[i];
}

void			md5(unsigned char *msg, size_t msg_len)
{
	unsigned long	state[4];
	unsigned int	i;
	char			*digest;

	md5_init(state);
	msg = msg_padding(msg, &msg_len, MD5_CHUNK_SIZE, LITTLE);
	i = 0;
	while (i < msg_len)
	{
		md5_transform(state, (const unsigned int *)(msg + i));
		i += MD5_CHUNK_SIZE / 8;
	}
	ft_memdel((void **)&msg);
	digest = strcat_digest(state, 8, 4, LITTLE);
	ft_printf("%s", digest);
	ft_strdel(&digest);
}
