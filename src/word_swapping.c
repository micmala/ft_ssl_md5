/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   word_swapping.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 17:14:05 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 17:14:07 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

void	sha512_swap_words(
		unsigned long *vars, unsigned long temp1, unsigned long temp2)
{
	vars[7] = vars[6];
	vars[6] = vars[5];
	vars[5] = vars[4];
	vars[4] = vars[3] + temp1;
	vars[3] = vars[2];
	vars[2] = vars[1];
	vars[1] = vars[0];
	vars[0] = temp1 + temp2;
}

void	sha256_swap_words(
		unsigned int *vars, unsigned int temp1, unsigned int temp2)
{
	vars[7] = vars[6];
	vars[6] = vars[5];
	vars[5] = vars[4];
	vars[4] = vars[3] + temp1;
	vars[3] = vars[2];
	vars[2] = vars[1];
	vars[1] = vars[0];
	vars[0] = temp1 + temp2;
}

void	md5_swap_words(unsigned int *vars, unsigned rotleft)
{
	unsigned int temp;

	temp = vars[3];
	vars[3] = vars[2];
	vars[2] = vars[1];
	vars[1] = vars[1] + rotleft;
	vars[0] = temp;
}
