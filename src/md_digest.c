/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md_digest.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 17:15:16 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 17:15:17 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

char	*hex_to_str(size_t value, int precision)
{
	char	*ret;

	ret = ft_strnew((size_t)precision);
	while (precision--)
	{
		ret[precision] = (char)(value % 16 +
				(value % 16 < 10 ? '0' : 'a' - 10));
		value /= 16;
	}
	return (ret);
}

char	*get_little_endian(char *src)
{
	char	*dest;
	size_t	len;
	size_t	i;

	len = ft_strlen(src);
	dest = ft_strnew(len);
	i = 0;
	while (i < len)
	{
		dest[i] = (char)(src[len - i - 2] ? src[len - i - 2] : '0');
		dest[i + 1] = src[len - i - 1];
		i += 2;
	}
	ft_strdel(&src);
	return (dest);
}

char	*strcat_digest(unsigned long *state, unsigned int word_len,
		unsigned int h_count, int endian)
{
	char				*res;
	char				*temp;
	unsigned int		i;

	res = ft_strnew(h_count * word_len);
	i = 0;
	while (i < h_count)
	{
		temp = hex_to_str(state[i], word_len);
		if (endian == LITTLE)
			temp = get_little_endian(temp);
		res = ft_strcat(res, temp);
		ft_strdel(&temp);
		i++;
	}
	return (res);
}
