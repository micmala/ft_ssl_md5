/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 17:15:24 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 17:15:25 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

t_md	bzero_handler(void)
{
	t_md new;

	new.funct_name = NULL;
	new.f_ptr = NULL;
	new.filename = NULL;
	new.msg = NULL;
	new.msg_len = 0;
	new.md_flags.s = false;
	new.md_flags.p = 0;
	new.md_flags.q = false;
	new.md_flags.r = false;
	new.md_flags.has_stdin = false;
	new.md_flags.has_arg = false;
	return (new);
}

void	handle_redundant_flag(t_md *handler)
{
	handler->msg = (unsigned char *)ft_strdup("");
	handler->msg_len = 0;
	print_flag_p(handler);
}

void	md_read_from_fd(int fd, t_md *handler, size_t buf_size)
{
	void	*buf;
	ssize_t	bytes;

	buf = ft_memalloc(buf_size);
	handler->msg = ft_memalloc(0);
	handler->msg_len = 0;
	while ((bytes = read(fd, buf, buf_size)) > 0)
	{
		handler->msg = ft_memjoin(handler->msg, buf,
				handler->msg_len, (size_t)bytes);
		handler->msg_len += bytes;
		ft_bzero(buf, buf_size);
	}
	ft_memdel(&buf);
}

void	ssl_string_option(t_md *handler, char ***argv)
{
	if (*(*argv + 1))
	{
		md_parse_msg(handler, *(*argv + 1));
		(*argv)++;
	}
	else
		ft_ssl_flag_err(handler->funct_name, 's', ERR_ARG);
}

void	ssl_p_option(t_md *handler)
{
	if (handler->md_flags.p > 1)
		handle_redundant_flag(handler);
	else if (handler->md_flags.p == 1 && !handler->md_flags.has_stdin)
	{
		handler->md_flags.has_stdin = true;
		md_read_from_fd(STDIN_FILENO, handler, READ_BUF);
		print_flag_p(handler);
	}
}
