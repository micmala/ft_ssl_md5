/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_error.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 17:19:32 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 17:19:33 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_ssl_error.h>

void	ft_ssl_usage(char *wrong_funct)
{
	int i;

	ft_dprintf(STDERR_FILENO,
			"ft_ssl: Error: '%s' is an invalid command.\n\n", wrong_funct);
	ft_dprintf(STDERR_FILENO, "Standart commands:\n\n");
	ft_dprintf(STDERR_FILENO, "Message Digest commands:\n");
	i = 0;
	while (g_md_cmds[i].name)
		ft_dprintf(STDERR_FILENO, "%s\n", g_md_cmds[i++].name);
	ft_dprintf(STDERR_FILENO, "\nCipher commands:\n\n");
	exit(EXIT_SUCCESS);
}

void	ft_ssl_flag_err(char *funct, char flag, char *err)
{
	char	*low_case_funct;
	int		i;

	low_case_funct = ft_strnew(ft_strlen(funct));
	i = -1;
	while (funct[++i])
		low_case_funct[i] = (char)ft_tolower(funct[i]);
	ft_dprintf(STDERR_FILENO, "%s: %s -- %c\n", low_case_funct, err, flag);
	ft_dprintf(STDERR_FILENO,
			"usage: %s [-pqr] [-s string] [files ...]\n", low_case_funct);
	ft_strdel(&low_case_funct);
	exit(EXIT_FAILURE);
}

void	ft_ssl_file_err(char *funct, char *filename, char *err)
{
	while (ft_isalpha(*funct))
		ft_dprintf(STDERR_FILENO, "%c", *funct++ + 32);
	ft_dprintf(STDERR_FILENO, "%s: %s: %s\n", funct, filename, err);
}
