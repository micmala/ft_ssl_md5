/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 16:58:49 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 16:58:50 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

void	sha384_init(unsigned long *state)
{
	state[0] = 0xcbbb9d5dc1059ed8;
	state[1] = 0x629a292a367cd507;
	state[2] = 0x9159015a3070dd17;
	state[3] = 0x152fecd8f70e5939;
	state[4] = 0x67332667ffc00b31;
	state[5] = 0x8eb44a8768581511;
	state[6] = 0xdb0c2e0d64f98fa7;
	state[7] = 0x47b5481dbefa4fa4;
}

void	sha384(unsigned char *msg, size_t msg_len)
{
	char			*digest;
	unsigned long	state[8];
	unsigned int	i;

	sha384_init(state);
	msg = msg_padding(msg, &msg_len, SHA512_CHUNK_SIZE, BIG);
	i = 0;
	while (i < msg_len)
	{
		sha512_transform(state, msg + i);
		i += SHA512_CHUNK_SIZE / 8;
	}
	ft_memdel((void **)&msg);
	digest = strcat_digest(state, 16, SHA384_STATE_TO_PRINT, BIG);
	ft_printf("%s", digest);
	ft_strdel(&digest);
}
