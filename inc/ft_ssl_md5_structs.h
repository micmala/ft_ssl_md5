/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_md5_structs.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 20:33:48 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 20:33:51 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_MD5_STRUCTS_H
# define FT_SSL_MD5_STRUCTS_H
# include <stdbool.h>
# include <stdint.h>
# include <string.h>

typedef struct	s_md_flags
{
	bool	s;
	int		p;
	bool	q;
	bool	r;
	bool	has_stdin;
	bool	has_arg;
}				t_md_flags;

typedef struct	s_md
{
	unsigned char	*msg;
	size_t			msg_len;
	t_md_flags		md_flags;
	char			*filename;
	char			*funct_name;
	void			(*f_ptr)(unsigned char *msg, size_t msg_len);
}				t_md;

#endif
