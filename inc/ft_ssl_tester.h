//
// Created by Mykola MALANCHUK on 10/9/18.
//

#ifndef FT_SSL_MD5_FT_SSL_TESTER_H
#define FT_SSL_MD5_FT_SSL_TESTER_H
# define TEST_CASES 77
# define OPENSSL_ERR 99
# include "ft_ssl_md5.h"
void test_md5(int ACTION);
#endif //FT_SSL_MD5_FT_SSL_TESTER_H
