/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_md5_utils.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 20:37:15 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 20:37:16 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_MD5_UTILS_H
# define FT_SSL_MD5_UTILS_H

# include "ft_ssl_md5_structs.h"

t_md	bzero_handler();
void	md_start(t_md *handler, char **argv);
void	md_read_from_fd(int fd, t_md *handler, size_t buf_size);
void	handle_redundant_flag(t_md *handler);
void	md_parse_msg(t_md *handler, char *arg);
void	ssl_string_option(t_md *handler, char ***argv);
void	ssl_p_option(t_md *handler);
char	*strcat_digest(unsigned long *state, unsigned int word_len,
		unsigned int h_count, int endian);
char	*hex_to_str(size_t value, int precision);

void	print_flag_s(t_md *handler);
void	print_file(t_md *handler);
void	print_flag_p(t_md *handler);

#endif
