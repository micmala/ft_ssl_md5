/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_md5.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 20:38:13 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 20:38:14 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_MD5_H
# define FT_SSL_MD5_H
# include <libft.h>
# include <ft_printf/ft_printf.h>
# include "ft_ssl.h"
# include "ft_ssl_md5_structs.h"
# include "ft_ssl_error.h"
# include "ft_ssl_md5_utils.h"

# define READ_BUF 4096

# define BIG 45
# define LITTLE 54

# define SHA224_STATE_TO_PRINT 7
# define SHA256_STATE_TO_PRINT 8
# define SHA384_STATE_TO_PRINT 6
# define SHA512_STATE_TO_PRINT 8

# define MD5_CHUNK_SIZE 512
# define SHA256_CHUNK_SIZE 512
# define SHA512_CHUNK_SIZE 1024

# define CH(x,y,z) (((x) & (y)) ^ (~(x) & (z)))
# define MAJ(x,y,z) (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))

void			md5(unsigned char *msg, size_t msg_len);
void			sha224(unsigned char *msg, size_t msg_len);
void			sha256(unsigned char *msg, size_t msg_len);
void			sha384(unsigned char *msg, size_t msg_len);
void			sha512(unsigned char *msg, size_t msg_len);

void			sha256_transform(
		unsigned long *state, const unsigned char *data);
void			sha512_transform(
		unsigned long *state, const unsigned char *data);

unsigned char	*msg_padding(unsigned char *msg, size_t *size,
		size_t chunk_size, int endian);

void			sha512_swap_words(
		unsigned long *vars, unsigned long temp1, unsigned long temp2);
void			sha256_swap_words(
		unsigned int *vars, unsigned int temp1, unsigned int temp2);
void			md5_swap_words(unsigned int *vars, unsigned rotleft);

#endif
