/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_error.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 20:32:54 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 20:32:55 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_ERROR_H
# define FT_SSL_ERROR_H
# include "ft_ssl_md5.h"
# include <errno.h>

# define ERR_ARG "option requires an argument"
# define ERR_OPT "illegal option"

void	ft_ssl_usage(char *wrong_funct);
void	ft_ssl_flag_err(char *funct, char flag, char *err);
void	ft_ssl_file_err(char *funct, char *filename, char *err);

#endif
