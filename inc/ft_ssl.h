/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 20:30:14 by mmalanch          #+#    #+#             */
/*   Updated: 2018/10/24 20:30:16 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_H
# define FT_SSL_H
# include <string.h>

typedef struct	s_dispatcher
{
	char	*name;
	void	(*f_ptr)(char **argv);
}				t_dispatcher;

extern t_dispatcher g_md_cmds[];
extern t_dispatcher g_cipher_cmds[];
extern t_dispatcher	g_stndrt_cmds[];

void			ssl_dispatcher(char **argv);

void			manage_md5(char **argv);
void			manage_sha256(char **argv);
void			manage_sha224(char **argv);
void			manage_sha512(char **argv);
void			manage_sha384(char **argv);
#endif
